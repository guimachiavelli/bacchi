var bacchiMap = {
	map: '',
	markers: [],
	center: new google.maps.LatLng(52.118759,5.40633),

	mapOptions: {
			center: new google.maps.LatLng(52.118759,5.40633),
			zoom: 10,
			disableDefaultUI: true,
			scrollwheel: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControl: true
	},

	mapStyle: [
		{
			"stylers": [
				{ "weight": 0.5 },
				{ "gamma": 0.7 },
				{ "hue": "#ded6b9"}
			]
		},{
			"elementType": "labels.icon",
			"stylers": [
				{ "visibility": "off" }
			]
		},{
			"featureType": "water",
			"stylers": [
				{ "lightness": 100 }
			]
		}
	],


	initializeMap: function() {
		var _this = this,
			latitude, longitude, latLng;

		if ($('#bacchi-map').length > 0) {
			this.map = new google.maps.Map(document.getElementById('bacchi-map'), this.mapOptions);
			this.map.setOptions({styles: this.mapStyle});
			this.placeLocations();
		}
		

	},

	placeLocations: function() {
		var _this = this,
			url = '';

        url = 'data/locations.json';

		$.ajax({
			url: url,
			dataType: 'json',

			success: function(data) {
				_this.setLocations(data);
            }
		});
	
	},


	setMarkers: function(lat, lng, icon, the_map) {
		var _this = this;
		if (!the_map) {
			the_map = _this.map;
		}

		latLng = new google.maps.LatLng(lat, lng);
		
		marker = new google.maps.Marker({
			position: latLng,
			map: the_map
		});
		
		_this.markers.push(marker);

		return marker;
	
	},

	setLocations: function(json_data) {
		var _this = this,
			latLng,
			info_object;

		$.each(json_data, function(index, value) {
			var	location_info_text,
				list_class;

			info_object = value.locations;

			_this.setMarkers(info_object.lat, info_object.lng);


			location_info_text =	'<div class="map-info">' +
									' 	<h1 class="map-title">' + info_object.title + '</h1>' +
									' 	<p class="map-description">' + info_object.body + '</p>' +
									'</div>';


			google.maps.event.addListener(marker, 'click', function() {
				location_info = new google.maps.InfoWindow({
					content: location_info_text,
					maxWidth: 200
				});
  				
				location_info.open(_this.map,this);
			});
		});
	}
};

bacchiModals = {

	simple_close: '<a class="close-map" href="#">✖</a>',
	close_button: '<a class="close-map with-overlay" href="#">✖</a>',

	loadModal: function(modal) {
		var father_coordinates;
		
		if ($('.modal').length > 0) {
			$('.modal').remove();
		}

		$('body').addClass('no-scroll').append('<div class="overlay"><div class="internal-padding"></div></div>');
		$('.overlay').hide().fadeIn('fast');
		$('.internal-padding').append(modal);
		$('.internal-padding').children('.modal').prepend(this.close_button);
		this.closeModal();

	},

	closeModal: function() {
		$('.close-map.with-overlay').click(function(){
			$(this).parent().fadeOut();
			$(this).parents('.overlay').fadeOut('slow', function() { this.remove(); });
			$('body').removeClass('no-scroll');

			return false;
		});

		$(document).keyup(function(e) {
  			if (e.keyCode == 27) {
				$('.modal').fadeOut();
				$('.overlay').fadeOut('slow', function() { this.remove(); });
				$('body').removeClass('no-scroll');
			}
		});
	}

}


$(document).ready(function(){
	bacchiMap.initializeMap();

	$('.default-form').validate();
	
	$('.carousel:not(.video-carousel)').jcarousel({
		'list': '.carousel-list',
		'wrap': 'circular',
	}).jcarouselAutoscroll({
		'interval': 8000
    });

	$('.video-carousel').jcarousel({
		'list': '.carousel-list',
		'wrap': 'both',
	});


	$('.btn-prev').click(function() {
		$(this).parent().siblings('.carousel').jcarousel('scroll', '-=1');
		return false;
	});

	$('.btn-next').click(function() {
		$(this).parent().siblings('.carousel').jcarousel('scroll', '+=1');
		return false;
	});

	$('#calculator select').dropkick({ width: 80 });
	$('#date-creator select').dropkick({ width: 120 });
	$('.buy-form select').dropkick({ width: 97 });
	
	$('#date-creator').hide();
	$('#date-anchor').click(function() { 
		$('#date-creator').show('fast');
		return false;
	});
	
	$('#date-creator .close').click(function() { 
		$('#date-creator').hide('fast');
	});

	$('.our-coffee #video .carousel-list a').click(function(){
		var video_url = $(this).attr('href').split('?v='), video_frame;
		video_url = video_url[1];

		video_frame = '<iframe id="current-video" width="589" height="263" src="http://www.youtube.com/embed/' + video_url + '?rel=0&amp;showinfo=0&amp;autohide=1&amp;controls=0&amp;iv_load_policy=3"></iframe>';

		$('#current-video').remove();
		$('#video .title').after(video_frame);	

		return false;
	});



	$('#store-locator a').click(function() {
		var modal_content = $(this).siblings('#bacchi-map').clone().addClass('modal');
		bacchiModals.loadModal(modal_content);
		return false;
	});

});
